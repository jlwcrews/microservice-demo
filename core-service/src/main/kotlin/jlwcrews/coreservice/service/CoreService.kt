package jlwcrews.coreservice.service

import jlwcrews.coreservice.gateway.CatGateway
import jlwcrews.coreservice.model.Cat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CoreService(@Autowired private val catGateway: CatGateway) {

    fun getCatById(id: Int): Cat? {
        return catGateway.fetchCat(id)
    }
}