package jlwcrews.coreservice.model

data class Cat(
    val id: Int,
    val name: String,
    val breed: String
)