package jlwcrews.coreservice.exception

import java.lang.RuntimeException

class CatNotFoundException(message: String?) : RuntimeException(message)