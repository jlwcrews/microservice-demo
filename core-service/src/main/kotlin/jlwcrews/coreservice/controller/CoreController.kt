package jlwcrews.coreservice.controller

import jlwcrews.coreservice.exception.CatNotFoundException
import jlwcrews.coreservice.model.Cat
import jlwcrews.coreservice.service.CoreService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.http.ResponseEntity

@RestController
@RequestMapping("/api")
class CoreController(@Autowired private val coreService: CoreService) {

    @GetMapping("/cat/{id}")
    fun getCatById(@PathVariable id: Int): ResponseEntity<Cat> {
        coreService.getCatById(id)?.let { return ResponseEntity.ok(it) }
        throw CatNotFoundException("No cat found matching your request")
    }
}