package jlwcrews.coreservice.gateway

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker
import jlwcrews.coreservice.exception.ServiceInterruptionException
import jlwcrews.coreservice.model.Cat
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class CatGateway {

    val logger = LoggerFactory.getLogger(CatGateway::class.java)
    val GET_ENDPOINT = "/cat"

    @Value("\${catservice.baseurl}")
    lateinit var baseUrl: String

    @CircuitBreaker(name = "catGateway", fallbackMethod = "cbFallback")
    fun fetchCat(id: Int): Cat? {
        val result = RestTemplate().getForObject("$baseUrl/$GET_ENDPOINT/$id", ByteArray::class.java)
        result?.let {
            return jacksonObjectMapper().readValue(result, Cat::class.java)
        }
        return null
    }

    fun cbFallback(e: Throwable): Cat? {
        logger.error("Circuit breaker fallback called")
        throw ServiceInterruptionException("The gateway is currently down, please try again later")
    }
}