package jlwcrews.coreservice

import jlwcrews.coreservice.extension.TestContainerExtension
import jlwcrews.coreservice.extension.WireMockExtension
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@SpringBootTest
@ExtendWith(TestContainerExtension::class, WireMockExtension::class)
@AutoConfigureMockMvc
@ActiveProfiles("integrationtest")
class CoreServiceApplicationTests(@Autowired private val mockMvc: MockMvc) {

    @Test
    fun shouldGetCatFromWiremock() {
        mockMvc
            .get("http://localhost:8080/api/cat/1")
            .andExpect { status { isOk() } }
            .andExpect { content { contentType(MediaType.APPLICATION_JSON) } }
            .andExpect { jsonPath("$.name", Matchers.`is`("Noodle")) }
            .andExpect { jsonPath("$.breed", Matchers.`is`("Kinda dumb")) }
    }

}
