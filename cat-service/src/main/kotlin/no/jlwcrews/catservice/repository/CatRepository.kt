package no.jlwcrews.catservice.repository

import no.jlwcrews.catservice.model.Cat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CatRepository: JpaRepository<Cat, Int>