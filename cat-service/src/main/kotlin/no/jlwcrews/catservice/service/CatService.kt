package no.jlwcrews.catservice.service

import no.jlwcrews.catservice.model.Cat
import no.jlwcrews.catservice.repository.CatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.CachePut
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
@CacheConfig(cacheNames = ["cats"])
class CatService(@Autowired private val catRepository: CatRepository) {

    @CacheEvict(allEntries = true)
    fun createCat(cat: Cat): Int? {
        return catRepository.save(cat).id
    }

    @CacheEvict(key = "#id")
    fun deleteCat(id: Int) {
        catRepository.deleteById(id)
    }

    @Cacheable(key = "#id")
    fun getCatById(id: Int): Cat? {
        return catRepository.findById(id).orElse(null)
    }

    fun getCats(): List<Cat> {
        return catRepository.findAll()
    }

    @CachePut(key = "#cat.id")
    fun updateCat(cat: Cat): Cat? {
        return catRepository.save(cat)
    }
}