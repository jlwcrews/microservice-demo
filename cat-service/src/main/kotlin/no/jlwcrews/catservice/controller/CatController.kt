package no.jlwcrews.catservice.controller

import no.jlwcrews.catservice.model.Cat
import no.jlwcrews.catservice.service.CatService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class CatController(@Autowired private val catService: CatService) {

    @GetMapping("/cat/{id}")
    fun getCatById(@PathVariable id: Int): ResponseEntity<Cat> {
        return ResponseEntity.ok(catService.getCatById(id))
    }

    @GetMapping("/cat")
    fun getCats(): ResponseEntity<List<Cat>> {
        return ResponseEntity.ok(catService.getCats())
    }

    @PostMapping("/cat")
    fun createCat(@RequestBody cat: Cat): ResponseEntity<Int> {
        return ResponseEntity.ok(catService.createCat(cat))
    }

    @DeleteMapping("/cat/{id}")
    fun deleteCat(@PathVariable id: Int): ResponseEntity<Unit> {
        return ResponseEntity.ok(catService.deleteCat(id))
    }
}