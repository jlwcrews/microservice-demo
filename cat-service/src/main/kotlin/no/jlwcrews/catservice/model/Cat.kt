package no.jlwcrews.catservice.model


import javax.persistence.*

@Entity
class Cat(
    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = "cat_id_sequence"
    )
    @SequenceGenerator(
        name = "cat_id_sequence",
        allocationSize = 1
    )
    val id: Int? = 0,
    val name: String,
    val breed: String
)