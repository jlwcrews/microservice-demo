CREATE TABLE cat
(
    id    INTEGER NOT NULL,
    name  VARCHAR(255),
    breed VARCHAR(255),
    CONSTRAINT pk_cat PRIMARY KEY (id)
);

create sequence cat_id_sequence;